package com.example.airplanning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeamprojectAirplanningApplication {

	public static void main(String[] args) {
		SpringApplication.run(TeamprojectAirplanningApplication.class, args);
	}

}
